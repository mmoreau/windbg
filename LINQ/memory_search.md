# String Memory Search

## **Searching for paths**

```
dx -r2 @$r = Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF C:\\Users").Where(x => x.Length != 0).Select(x => Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))), [@$r.Count()]
```

Only the data under the representation of a hexdump
```
dx -r2 @$r = Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF C:\\Users\\").Where(x => x.Length != 0).Select(x => Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17))).Select(x => x.Remove(0, 68))), [@$r.Count()]
```

## **Search for internet links**

### **HTTP / HTTPS**

```
dx -r2 @$r = Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF http").Where(x => x.Length != 0).Select(x => Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))), [@$r.Count()]
```

### **Youtube**

Beware, you may come across videos that you haven't even seen, clicked or searched.

```
dx -r2 @$r = Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF https://www.youtube.com/watch?v=").Where(x => x.Length != 0).Select(x => Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))), [@$r.Count()]
```

Or directly the links

```
dx -r1 @$r = Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF https://www.youtube.com/watch?v=").Where(x => x.Length != 0).Select(x => "https://www.youtube.com/watch?v=" + Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))[2].Remove(0, 68).Remove(11)), [@$r.Count()]
```

---

Keep the links avoiding duplicate content with the "**.Distinct()**" method.

**Warning** : If you see the sentence "**Cannot compares non-intrinsic values to each other**" on the screen, wait 2-3 seconds and then retype the command

```
dx -r1 @$r = (Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF https://www.youtube.com/watch?v=").Where(x => x.Length != 0).Select(x => "https://www.youtube.com/watch?v=" + Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))[2].Remove(0, 68).Remove(11))).Distinct(), [@$r.Count()]
```

Or write the command twice

```
dx -r0 Debugger.Utility.Control.ExecuteCommand("s -a 0x7FFFFFFFFFFFFFFF L?0x7FFFFFFFFFFFFFFF https://www.youtube.com/watch?v="), 0

dx -r1 @$r = (Debugger.Utility.Control.ExecuteCommand("s -a 0 L?0x7FFFFFFFFFFFFFFF https://www.youtube.com/watch?v=").Where(x => x.Length != 0).Select(x => "https://www.youtube.com/watch?v=" + Debugger.Utility.Control.ExecuteCommand("dB " + (x.Remove(17)))[2].Remove(0, 68).Remove(11))).Distinct(), [@$r.Count()]
```