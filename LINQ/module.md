# Module

## [User-Mode]

```
    dx @$cursession.Processes.First().Modules, [@$cursession.Processes.First().Modules.Count()]
```

```
    dx @$cursession.Processes.First().Modules.Select(m => m->BaseAddress.ToDisplayString("x") + " # " + m.ToDisplayString("su")), [@$cursession.Processes.First().Modules.Count()]
```