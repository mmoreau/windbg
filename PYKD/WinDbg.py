from pykd import dbgCommand

class WinDbg:


    @classmethod
    def CommandLine(cls):

        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->CommandLine; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)
        
        print(f"Command :\n\t{pykd_cmd}\n")

    

    @classmethod
    def CurrentDirectory(cls):
        
        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->CurrentDirectory.DosPath; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)

        print(f"CurrentDirectory :\n\t{pykd_cmd}\n")


    
    @classmethod
    def ImagePathName(cls):

        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->ImagePathName; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)

        print(f"ImagePathName :\n\t{pykd_cmd}\n")


    
    @classmethod
    def DllPath(cls):

        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->DllPath; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)

        print(f"DllPath :\n\t{pykd_cmd}\n")

    

    @classmethod
    def WindowTitle(cls):

        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->WindowTitle; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)

        print(f"WindowTitle :\n\t{pykd_cmd}\n")



    @classmethod
    def DesktopInfo(cls):

        windbg_cmd = 'r? $t0 = &@$peb->ProcessParameters->DesktopInfo; r? $t1 = @@C++(@$t0->Buffer) ; .if (@@C++(@$t1)) {.printf "%mu", @@C++(@$t1)} .else {.printf "<Unknown>"}'
        pykd_cmd = dbgCommand(windbg_cmd)

        print(f"DesktopInfo :\n\t{pykd_cmd}\n")



    @classmethod
    def Modules(cls):

        windbg_cmd = '!list -t ntdll!_LDR_DATA_TABLE_ENTRY.InLoadOrderLinks.Flink -x "dt ntdll!_LDR_DATA_TABLE_ENTRY FullDllName @$extret" @@C++(((ntdll!_PEB*)@$proc)->Ldr->InLoadOrderModuleList.Flink)'
        pykd_cmd = dbgCommand(windbg_cmd)

        [print(module.split("_UNICODE_STRING")[-1][2:-1]) for module in pykd_cmd.split("\n\n")[:-1]]
            
    

    @classmethod
    def ProcessHost(cls):

        windbg_cmd = '.tlist'
        pykd_cmd = dbgCommand(windbg_cmd)

        for process in pykd_cmd.split("\n")[:-1]:
            data = process.strip(" ").split(" ")
            print(f"[{data[0][2:]}] {data[1]}")




    @classmethod
    def PowershellHistory(cls):

        windbg_cmd = """.loadby sos clr; .symfix; .reload; dx @$getObject = (x => Debugger.Utility.Control.ExecuteCommand("!do /d " + (x).ToDisplayString("x"))); dx Debugger.Utility.Control.ExecuteCommand("!DumpHeap -Type HistoryInfo -short").Flatten(n => n).Where(x => @$getObject(x)[0].Remove(0, 13) == "Microsoft.PowerShell.Commands.HistoryInfo").Select(x => @$getObject(@$getObject(x)[9].Remove(0, 68).Remove(16))[5].Remove(0, 13)).Distinct()"""
        pykd_cmd = dbgCommand(windbg_cmd)

        [print(command.split(":")[-1][1:]) for command in pykd_cmd.split("\n")[6:-1]]



"""
WinDbg.CommandLine()
WinDbg.CurrentDirectory()
WinDbg.ImagePathName()
WinDbg.DllPath()
WinDbg.WindowTitle()
WinDbg.DesktopInfo()
WinDbg.Modules()
WinDbg.ProcessHost()
WinDbg.PowershellHistory()
"""
