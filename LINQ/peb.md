## [User-Mode]


### PEB

```
dx @$pebObj = @$peb, new {ImageBaseAddress = Debugger.Utility.Control.ExecuteCommand("?? @$peb->ImageBaseAddress").First().ToDisplayString("su").Remove(0, 7), OSMajorVersion = ((int)@$pebObj->OSMajorVersion).ToDisplayString("d"), OSMinorVersion = ((int)@$pebObj->OSMinorVersion).ToDisplayString("d"), OSBuildNumber = ((int)@$pebObj->OSBuildNumber).ToDisplayString("d"), OSCSDVersion = ((int)@$pebObj->OSCSDVersion).ToDisplayString("d"), OSPlatformId = ((int)@$pebObj->OSPlatformId).ToDisplayString("d"), ImageSubsystem = ((int)@$pebObj->ImageSubsystem).ToDisplayString("d"), ImageSubsystemMajorVersion = ((int)@$pebObj->ImageSubsystemMajorVersion).ToDisplayString("d"), ImageSubsystemMinorVersion = ((int)@$pebObj->ImageSubsystemMinorVersion).ToDisplayString("d")}
```

### ProcessParameters

```
dx @$us2str = ((x) => (x.Length > 0 ? x.Buffer.ToDisplayString("su"): "\"\""))

dx @$r = @$peb->ProcessParameters, new {CommandLine = @$us2str(@$r->CommandLine), ImagePathName = @$us2str(@$r->ImagePathName), CurrentDirectory = @$us2str(@$r->CurrentDirectory.DosPath), DllPath = @$us2str(@$r->DllPath), WindowTitle = @$us2str(@$r->WindowTitle), DesktopInfo = @$us2str(@$r->DesktopInfo), ShellInfo = @$us2str(@$r->ShellInfo), RuntimeData = @$us2str(@$r->RuntimeData)}
```

## [Kernel-Mode]

### PEB 

```
dx -r2 (Debugger.Utility.Collections.FromListEntry(*(nt!_LIST_ENTRY*)&nt!HandleTableListHead, "nt!_HANDLE_TABLE", "HandleTableList")).Where(x => x.QuotaProcess != 0 && x.QuotaProcess->Peb != 0).Select(x => Debugger.Utility.Control.ExecuteCommand(".process /p " + ((__int64)x.QuotaProcess).ToDisplayString("x") + "; dx @$pebObj = @$peb, new {ImageBaseAddress = @$peb->ImageBaseAddress & ~0xF, OSMajorVersion = ((int)@$pebObj->OSMajorVersion).ToDisplayString(\"d\"), OSMinorVersion = ((int)@$pebObj->OSMinorVersion).ToDisplayString(\"d\"), OSBuildNumber = ((int)@$pebObj->OSBuildNumber).ToDisplayString(\"d\"), OSCSDVersion = ((int)@$pebObj->OSCSDVersion).ToDisplayString(\"d\"), OSPlatformId = ((int)@$pebObj->OSPlatformId).ToDisplayString(\"d\"), ImageSubsystem = ((int)@$pebObj->ImageSubsystem).ToDisplayString(\"d\"), ImageSubsystemMajorVersion = ((int)@$pebObj->ImageSubsystemMajorVersion).ToDisplayString(\"d\"), ImageSubsystemMinorVersion = ((int)@$pebObj->ImageSubsystemMinorVersion).ToDisplayString(\"d\")}").Skip(2)), [Debugger.Utility.Collections.FromListEntry(*(nt!_LIST_ENTRY*)&nt!HandleTableListHead, "nt!_HANDLE_TABLE", "HandleTableList").Count()]
```

### ProcessParameters 

```
dx @$us2str = ((x) => (x.Length > 0 ? x.Buffer.ToDisplayString("su"): "\"\""))

dx -r2 (Debugger.Utility.Collections.FromListEntry(*(nt!_LIST_ENTRY*)&nt!HandleTableListHead, "nt!_HANDLE_TABLE", "HandleTableList")).Where(x => x.QuotaProcess != 0 && x.QuotaProcess->Peb != 0).Select(x => Debugger.Utility.Control.ExecuteCommand(".process /p " + ((__int64)x.QuotaProcess).ToDisplayString("x") + "; dx @$r = @$peb->ProcessParameters, new {CommandLine = @$us2str(@$r->CommandLine), ImagePathName = @$us2str(@$r->ImagePathName), CurrentDirectory = @$us2str(@$r->CurrentDirectory.DosPath), DllPath = @$us2str(@$r->DllPath), WindowTitle = @$us2str(@$r->WindowTitle), DesktopInfo = @$us2str(@$r->DesktopInfo), ShellInfo = @$us2str(@$r->ShellInfo), RuntimeData = @$us2str(@$r->RuntimeData)}").Skip(2)), [Debugger.Utility.Collections.FromListEntry(*(nt!_LIST_ENTRY*)&nt!HandleTableListHead, "nt!_HANDLE_TABLE", "HandleTableList").Count()]
```