# WinDbg

Script WinDbg (.wds) / PyKd Python 3

## PyKd

Download the pykd.dll in a zip folder 
* https://githomelab.ru/pykd/pykd-ext/-/wikis/Downloads

Installing PyKd with Python 3

```
    pip install pykd --user
```

Commands to type in WinDbg

```
    .load C:\Users\MyUserName\Downloads\pykd_ext_2.0.0.24\x64\pykd.dll
    !py C:\Users\MyUserName\Desktop\WinDbg.py
```

The examples are in the PyKd folder

## Script .wds

Commands to type in WinDbg

```
    $$><C:\Users\MyUserName\Desktop\winpeb.wds
```

Examples can be found in the **WDS** folder

## Todo

* [ ] LINQ

## Links

* https://pypi.org/project/pykd/
* https://githomelab.ru/pykd/pykd-ext/-/wikis/Downloads